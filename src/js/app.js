import $ from 'jquery';

jQuery(document).ready(function ($) {

	let cardsBlocks = $('.dn-cards');
	let btnsCards = $('.card-name');

	let addAttrs = (elements, dataName) => {
		$.each(elements, function (indexData) {
			$(this).attr(dataName, indexData);
		});
	};

	addAttrs(cardsBlocks, 'data-card-block');
	addAttrs(btnsCards, 'data-card-btn');

	btnsCards.on('click', function (event) {
		event.preventDefault();

		if ($(document).width() < 768) {
			let dataBtn = $(this).data('card-btn');
			let blockCards = $(document).find(`[data-card-block=${dataBtn}]`);

			btnsCards.removeClass('active');

			if (blockCards.css('display') == 'none') {
				$(this).toggleClass('active');
				cardsBlocks.hide(0);
				blockCards.fadeIn('fast');

				let scrollToBnt = $(this).offset().top;
				let body = $('html, body');
				body.animate({scrollTop:scrollToBnt - 50}, 500, 'swing');
			} else {
				blockCards.hide(0);
			}
		} else {
			let dataBtn = $(this).data('card-btn');
			let blockCards = $(document).find(`[data-card-block=${dataBtn}]`);
			blockCards.show('fast');
			$(this).removeClass('active');
		}
	});


});
